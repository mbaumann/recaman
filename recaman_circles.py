"""Plot some awesome circles (they are based on the Recaman sequence)
"""
import time
import numpy as np
import copy
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge, Arc


N_MAX = 1024

class DataHandler():
    "Main Class for generating and storing the sequence"

    def __init__(self, series):
        self.series = series

    def calculateNextNumbers(self, n_to_calculate=1):
        """Calculate nex numbers in the sequence

        Keyword Arguments:
            n_to_calculate {int} -- amount of numbers to calculate (default: {1})
        """
        n = len(self.series)
        for i in range(n, n + n_to_calculate):
            nextNumberCandidate = self.series[-1] - i
            if nextNumberCandidate > 0 and nextNumberCandidate not in self.series:
                self.series.append(nextNumberCandidate)
            else:
                self.series.append(self.series[-1] + i)

    def getSeries(self):
        """return series
        """
        return self.series

    def printAll(self):
        """Print series
        """
        print(self.series)


class Plotter():
    """Class to plot series
    """

    def __init__(self, series):
        self.series = series
        self.n_plotted = 0

        self.fig = None
        self.ax = None
        self.config = {'width': 0.7}

    def createPlot(self):
        """Create initial plot
        """
        self.fig, self.ax = plt.subplots(figsize=(8, 8))
        self.ax.axis('equal')
        self.ax.axis('off')
        self.fig.canvas.manager.window.state('zoomed')

    def plotAll(self, finalZoomLevel=False):
        """Plot series given as input
        """
        if not self.fig:
            self.createPlot()

        n = len(self.series)

        for i in range(self.n_plotted, n - 1):
            midpoint = (self.series[i + 1] + self.series[i]) / 2
            radius = abs(self.series[i + 1] - self.series[i]
                         ) / 2 + self.config['width'] / 2
            upOrDown = i % 2

            self.ax.add_patch(
            #     Arc(
            #         xy=(midpoint, 0),
            #         width=2*radius,
            #         height=2*radius,
            #         angle=180*upOrDown,
            #         theta1=0,
            #         theta2=180
            #     )
                Wedge(
                    (midpoint, 0),
                    radius,
                    180 * upOrDown,
                    180 * (upOrDown + 1),
                    width=self.config['width'],
                    # color=min(i / 256, 1) * np.array([1, 1, 1])))
                    color=max(1 - (i + 10 ) / (N_MAX-10), 0) * np.array([1, 1, 1])
                    )
                )

        #colors = 100*np.random.rand(len(patches))
        #p.set_array(np.array(colors))
        #self.ax.add_collection(PatchCollection(self.patches))
        #fig.colorbar(p, ax=ax)
        self.n_plotted = n - 1

        cur_lim = self.ax.get_xlim()[1]
        target_lim = max(self.series)

        if finalZoomLevel:
            new_lim = max(cur_lim + min(target_lim - cur_lim, cur_lim / 30), 10)
        else:
            new_lim = target_lim
        self.ax.set_xlim([-self.config['width'] / 2, new_lim])
        self.fig.canvas.draw_idle()
        self.fig.canvas.flush_events()


class Plotter_bokeh():
    """Class to plot series
    """

    def __init__(self, series):
        self.series = series
        self.n_plotted = 0

        self.fig = None
        self.ax = None
        self.config = {'width': 0.7}

    def createPlot(self):
        """Create initial plot
        """
        self.fig, self.ax = plt.subplots(figsize=(8, 8))
        self.ax.axis('equal')
        self.ax.axis('off')
        self.fig.canvas.manager.window.state('zoomed')

    def plotAll(self, finalZoomLevel=False):
        """Plot series given as input
        """
        if not self.fig:
            self.createPlot()

        n = len(self.series)

        for i in range(self.n_plotted, n - 1):
            midpoint = (self.series[i + 1] + self.series[i]) / 2
            radius = abs(self.series[i + 1] - self.series[i]
                         ) / 2 + self.config['width'] / 2
            upOrDown = i % 2

            self.ax.add_patch(
            #     Arc(
            #         xy=(midpoint, 0),
            #         width=2*radius,
            #         height=2*radius,
            #         angle=180*upOrDown,
            #         theta1=0,
            #         theta2=180
            #     )
                Wedge(
                    (midpoint, 0),
                    radius,
                    180 * upOrDown,
                    180 * (upOrDown + 1),
                    width=self.config['width'],
                    # color=min(i / 256, 1) * np.array([1, 1, 1])))
                    color=max(1 - (i + 10 ) / (N_MAX-10), 0) * np.array([1, 1, 1])
                    )
                )

        #colors = 100*np.random.rand(len(patches))
        #p.set_array(np.array(colors))
        #self.ax.add_collection(PatchCollection(self.patches))
        #fig.colorbar(p, ax=ax)
        self.n_plotted = n - 1

        cur_lim = self.ax.get_xlim()[1]
        target_lim = max(self.series)

        if finalZoomLevel:
            new_lim = max(cur_lim + min(target_lim - cur_lim, cur_lim / 30), 10)
        else:
            new_lim = target_lim
        self.ax.set_xlim([-self.config['width'] / 2, new_lim])
        self.fig.canvas.draw_idle()
        self.fig.canvas.flush_events()


class Recaman():
    """Main class
    """

    def __init__(self):
        self.series = [0]
        self.dh = DataHandler(self.series)
        self.pt = Plotter(self.series)


recaman = Recaman()


def plot_endless():
    # endless plotting
    counter = 0
    steps = 1
    while (1):
        recaman.dh.calculateNextNumbers(steps)
        recaman.pt.plotAll()
        
        counter += 1

        if counter >= N_MAX:
            pass
            # break
        if counter % 1000 == 0:
            print(len(recaman.series))
            steps *= 2

def plot_once():
    recaman.dh.calculateNextNumbers(N_MAX)  
    recaman.pt.plotAll()
    
    plt.show()

#plot_once()

def bokeh_test():
    import numpy as np
    from bokeh.plotting import figure, show, save, output_notebook, output_file
    from bokeh.layouts import gridplot
    #output_notebook()
    output_file("test.html")
    from bokeh.models import ColumnDataSource


    N = 1
    x = np.linspace(-2, 2, N)
    y = x**2

    data = {'x': x,
    'y':y}

    source = ColumnDataSource(data=data)

    p1 = figure(title="annular_wedge")
    p1.annular_wedge(x='x', y='y', inner_radius=100, outer_radius=200, start_angle=0.6, end_angle=4.1, color="#8888ee",
        inner_radius_units="screen", outer_radius_units="screen", alpha=0.6, source=source)
        #p1.annular_wedge(x='x', y='y', 100, 200, 0.6, 4.1, color="#8888ee",
#        inner_radius_units="screen", outer_radius_units="screen", alpha=0.6, source=source)


    #p2 = figure(title="arc")
    #p2.arc(x, y, 200, 1, 4.1,
    #    radius_units="screen", color="#BEAED4", line_width=30, alpha=0.6)
    
    show(p1)

    for i in range(10):
        N = i
        x = np.linspace(-2, 2, N)
        y = x**2

        new_data = {'x': x,
        'y':y}

        source.stream(new_data)

        save(p1)

    

    #show(p2)

    #p2.arc(2*x,2*y, 200, 1, 4.1, radius_units="screen", color="#BEAED4", line_width=30, alpha=0.6)

    #show(p2)

bokeh_test()